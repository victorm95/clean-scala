name := "clean architecture"

organization := "victorm"

version := "0.0.0"

scalaVersion := "2.12.4"

mainClass := Some("clean.Main")
