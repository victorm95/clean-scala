package clean.gateway

import clean.entity._

abstract class Gateway[T<:Entity] {
  def add(entity: T): Unit
  def all: Seq[T]
  def update(entity: T): Unit
  def remove(entity: T): Unit
}

trait InMemoryGateway[T<:Entity] {
  var datastore: Seq[T]

  def add(entity: T): Unit =
    datastore = datastore :+ entity

  def all: Seq[T] = datastore

  def update(entity: T): Unit =
    datastore = datastore.map(e => if (e.id == entity.id) entity else e)

  def remove(entity: T): Unit =
    datastore = datastore.filter(_.id != entity.id)
}

class InMemoryProductGateway extends Gateway[Product] with InMemoryGateway[Product] {
  override var datastore: Seq[Product] = Vector[Product]()
}
