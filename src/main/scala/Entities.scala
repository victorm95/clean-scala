package clean.entity

abstract class Entity (val id: String)

case class Product(override val id: String, code: String, name: String, price: Float) extends Entity(id)
