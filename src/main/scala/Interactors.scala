package clean.interactor

import clean.entity._
import clean.gateway._

abstract class Interactor[T<:Entity]

class CreateProductInteractor (implicit gateway: Gateway[Product]) extends Interactor[Product] {
  def apply(product: Product): Unit = gateway.add(product)
}

class ListProductsInteractor (implicit gateway: Gateway[Product]) extends Interactor[Product] {
  def apply(): Seq[Product] = gateway.all
}
