package clean

import entity._
import gateway._
import interactor._

object Main {
  def main(args: Array[String]): Unit = {
    implicit val productGateway = new InMemoryProductGateway
    
    val createProduct = new CreateProductInteractor
    val listProducts = new ListProductsInteractor

    println(listProducts())
    createProduct(Product("0", "001", "Product 1", 10.0f))
    createProduct(Product("1", "002", "Product 2", 15.5f))
    println(listProducts())
  }
}
